.PHONY: all
all:

.PHONY: update_wsicilian
update_wsicilian: scnwiki-latest-pages-articles.xml include exclude
	./process scnwiki-latest-pages-articles.xml > $@

scnwiki-latest-pages-articles.xml:
	wget https://dumps.wikimedia.org/scnwiki/latest/scnwiki-latest-pages-articles.xml.bz2
	bunzip2 scnwiki-latest-pages-articles.xml.bz2

.PHONY: clean
clean:
	$(RM) scnwiki-latest-pages-articles.xml
	$(RM) *orig.tar.gz
	$(RM) -r deb-pkg

.PHONY: dist
dist: wsicilian
	cd ..; tar -czf sicilianu/sicilianu_0~`date -Idate | sed -e s/-//g`.orig.tar.gz \
		sicilianu/wsicilian \
		sicilianu/Makefile \
		sicilianu/dictcommon/ \
		sicilianu/LICENSE \
		sicilianu/process \
		sicilianu/README.md

.PHONY: install
install: wsicilian
	install -m644 -D wsicilian ${DESTDIR}/usr/share/dict/wsicilian
	install -m644 -D dictcommon/wsicilian ${DESTDIR}/var/lib/dictionaries-common/wordlist/wsicilian

.PHONY: deb-pkg
deb-pkg: dist
	$(RM) -r /tmp/sicilianu*
	mv sicilianu*orig* /tmp
	cd /tmp; tar -xf sicilianu*orig*.gz
	cp -r debian /tmp/sicilianu/
	cd /tmp/sicilianu; dpkg-buildpackage #--changes-option=-S
	mkdir -p deb-pkg
	mv /tmp/sicilianu*.* /tmp/wsicilian*.* deb-pkg
	lintian --pedantic -E --color auto -i -I deb-pkg/*changes deb-pkg/*deb
